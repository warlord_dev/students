package dev.warlord.students.repositories


import dev.warlord.students.models.Student
import org.springframework.data.repository.CrudRepository


interface StudentRepository : CrudRepository<Student?, Int?>