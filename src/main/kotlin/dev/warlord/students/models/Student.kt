package dev.warlord.students.models


import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*
import javax.persistence.*

@Entity
data class Student(
        @Id @GeneratedValue(strategy= GenerationType.AUTO) val id: Int? = null,

        @JsonProperty("name")val name: String,

        @JsonProperty("sure_name") val sureName: String,

        @JsonProperty("last_name") val lastName: String

        //val birthdayDate: Date
)