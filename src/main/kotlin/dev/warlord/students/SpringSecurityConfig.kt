package dev.warlord.students

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import javax.ws.rs.HttpMethod

@Configuration
class SpringSecurityConfig: WebSecurityConfigurerAdapter() {
    @Throws(Exception::class)
    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.inMemoryAuthentication()
                .withUser("user").password("{noop}password").roles("USER")
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http //HTTP Basic authentication
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/students/").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/students/list").hasRole("USER")
                .antMatchers(HttpMethod.PUT, "/students/update").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/students/create").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/students/delete").hasRole("USER")
                .and()
                .csrf().disable()
                .formLogin().disable()
    }
}