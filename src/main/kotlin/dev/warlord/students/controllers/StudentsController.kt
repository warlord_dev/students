package dev.warlord.students.controllers

import dev.warlord.students.models.Student
import dev.warlord.students.repositories.StudentRepository
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/students")
class StudentsController {

    @Inject
    lateinit var studentRepository: StudentRepository


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/")
    fun getStudentByID(@QueryParam("id") id: Int ): Response {
        return Response.ok(studentRepository.findById(id)).build()
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    fun getAllStudents(): Response {
       return Response.ok(studentRepository.findAll()).build()
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/delete")
    fun deleteStudent(@QueryParam("id") id: Int): Response {
        return Response.ok(studentRepository.deleteById(id)).build()
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/update")
    fun updateStudent(students: Array<Student>): Response {
        for (student in students) {
            studentRepository.save(student)
        }
        return Response.ok(studentRepository.findAll()).build()
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/create")
    fun createStudents(students: Array<Student> ): Response {
        for (x in students) {
            studentRepository.save(x)
        }
        return Response.ok("ok saved").build()
    }




}