package dev.warlord.students

import org.glassfish.jersey.server.ResourceConfig
import org.springframework.stereotype.Component

@Component
class JerseyConfig: ResourceConfig {
    constructor() {
        packages(JerseyConfig::class.java.`package`.name)
    }
}